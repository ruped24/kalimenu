# This menu is for easy access to kali's most used toolsets #

![](https://img.shields.io/badge/kalimenu-bash-green.svg?style=flat-square)

***


** Download zip: **

** [module_zero_installer.sh](https://gist.github.com/ruped24/1fea0fe758cbf29260ad/archive/7e3634f661f1f0ca0517716a5c16ff6feede7af2.zip) will install menu.sh, tools and dependencies. **

** [Usage:](https://www.youtube.com/watch?v=B2HH8ueiWxE) **
```
#!shell

sudo menu.sh
```




**[Screenshot](https://drive.google.com/file/d/0B79r4wTVj-CZWEkwcXFzWGFraWs)** :wink:

**The tools can be found below:**

1) Killchain –- https://github.com/ruped24/killchain

2) Fuckshitup –- https://github.com/ruped24/fuckshitup

3) Discover -- https://github.com/leebaird/discover

4) Netools –- http://sourceforge.net/projects/netoolsh

5) BBQSQL -- sudo pip install bbqsql

6) Lazykali –- https://github.com/ruped24/Lazykali

7) Tangodown -- http://tinyurl.com/pqagjyw



**Added tools not shown on menu:** 

Shellter

Backdoor-factory