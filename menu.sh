#!/bin/bash
#
# Written by Rupe

while true
do
    OPTION=$(whiptail --title "Toolsets Menu" \
      --menu "Choose your toolset" 15 60 7 \
      "1)" "Killchain" \
      "2)" "Fuckshitup" \
      "3)" "Discover" \
      "4)" "Netools" \
      "5)" "BBQSQL" \
      "6)" "Lazykali" \
      "7)" "Tangodown"  3>&1 1>&2 2>&3)

    exitstatus=$?

    if [ ${exitstatus} -eq '0' ]; then
      case ${OPTION} in
        "1)") killchain.py ;;
        "2)") cd /opt/metasploit/scripts/fsu && php -f fsu.php ;;
        "3)") cd /opt/discover && ./discover.sh ;;
        "4)") netool.sh ;;
        "5)") bbqsql ;;
        "6)") lazykali ;;
        "7)") cd /opt/metasploit/scripts && ./tangodown.sh ;;
      esac
    else
      echo "You chose Cancel."
      exit 1
    fi
done
