#! /bin/bash
#
# Written by Rupe 3-2-16
# Scriptkiddies menu.sh and tools installer script v.1.0


kill_chain() {
  cd /usr/local/bin && \
  git clone https://github.com/ruped24/killchain
  cd killchain && chmod +x ./killchain.py
  cd -
  ln -s ./killchain/killchain.py ./
  return $?
}

fuckshitup() {
  cd /usr/local/bin && \
  git clone https://github.com/ruped24/fuckshitup
  cd fuckshitup && chmod +x ./fsu.php
  cd -
  ln -s ./fuckshitup/fsu.php ./
  return $?
}

antivirus_bypass_extra() {
  apt-get update && \
  apt-get -y install goofile etherape \
  shellter backdoor-factory theharvester bbqsql \
  websploit openvas veil-evasion tor whois
  return $?
}

discover() {
  cd /usr/local/bin && \
  git clone https://github.com/leebaird/discover
  
  cd discover && \
  sed -i '/discover=$(locate/d' discover.sh
  sed -i '31i discover="/usr/local/bin/discover"' discover.sh
  sed -i 's/theHarvester/theharvester/g' discover.sh && \
  chmod + x discover.sh
  cd -
  
  ln -s ./discover/mods ./
  ln -s ./discover/report ./
  return $?
}

lazykali() {
  cd /tmp && \
  git clone https://github.com/ruped24/lazykali
  cd ./lazykali && \
  chmod +x Lazykali.sh && ./Lazykali.sh
  rm -rf "/tmp/Lazykali lazykali" &> /dev/null
  return $?
}

kali_menu() {
  menu='
  #! /bin/bash
  #
  # Written by Rupe 3-2-16
  # Skriptkiddies menu.sh
  
  while true
  do
    OPTION=$(whiptail --title "Toolsets Menu" \
      --menu "Choose your toolset" 15 60 7 \
      "1)" "Killchain" \
      "2)" "Fuckshitup" \
      "3)" "Discover" \
      "4)" "BBQSQL" \
    "5)" "Lazykali"  3>&1 1>&2 2>&3)
    
    exitstatus=$?
    
    if [ ${exitstatus} -eq '0' ]; then
      case ${OPTION} in
        "1)") killchain.py ;;
        "2)") cd /usr/local/bin && php -f fsu.php ;;
        "3)") /usr/local/bin/discover/discover.sh ;;
        "4)") bbqsql ;;
        "5)") lazykali ;;
      esac
    else
      echo "You chose Cancel."
      exit 1
    fi
  done'
  cd /usr/local/bin
cat - <<EOF > menu.sh
  $menu
EOF
  chmod +x menu.sh
  return $?
}
  
installem() {
  kill_chain
  fuckshitup
  antivirus_bypass_extra
  discover
  lazykali
  kali_menu
  return $?
}
  
confirm() {
  read -r -p "${1:-Are you sure? [y/n]} " answer
  case ${answer} in
    [yY][eE][sS]|[yY])
      installem
    ;;
    *)
      echo "You chose Cancel."
      exit 1
    ;;
  esac
}
  
if [[ $EUID -ne '0' ]]; then
  echo "[!] This script must be run as super user." 1>&2
  exit 1
fi
  
confirm
  
if [[ $? -eq '0' ]]; then
  cd ~/
fi
  
menu.sh

exit $?